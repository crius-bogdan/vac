const submits = document.querySelectorAll('.c-budget-form .step-control');
const progress = document.querySelector('.c-budget-form .c-form-progress-done');
let data = {};
let PrevStep = '';
for (const submit of submits) {
    submit.addEventListener('click', function (e) {
        e.preventDefault()
        let next = this.dataset.next;
        const current_name = this.dataset.current;

        const currentStep = document.querySelector('.c-budget-steps--active');

        const current_inputs = document.querySelectorAll(`input[name="${current_name}"]`)
        if(this.dataset.back) {
            next = PrevStep
        } else {
            PrevStep = current_name;
            for(let input of current_inputs) {
                if (input.type == 'radio' && current_name != 'own-home') {
                    if(input.checked) {
                        data[current_name] = input.value
                    }
                }
                if(input.type == 'text' || current_name == 'own-home') {


                    if(input.value.length > 0) {
                        if(typeof data[current_name] != 'object') data[current_name] = []
                        data[current_name].push(input.value)
                    }
                    if(input.checked) {
                        if(typeof data[current_name] != 'object') data[current_name] = []
                        data[current_name].push(input.value)
                    }
                }
            }

            if(!data.hasOwnProperty(current_name) && !this.dataset.dataBack) return
            // console.log(data)
            if(current_name == 'status') next = data[current_name] == 'employed' ? 'income-details' : data[current_name]
            //
            if(current_name == 'income-details') next = data[current_name]

            if(current_name == 'long-income') next = data['status'] == 'employed' ? 'tell-about-employed' : this.dataset.next

            if(data['status'] == 'student') {
                if(current_name == 'student') {
                    if (data[current_name] == 'yes') {
                        next = 'income-details'
                    } else {
                        next = 'monthly'
                    }
                }
            }

            if(current_name == 'last-step') {
                var ajaxurl = "/wp-admin/admin-ajax.php";

                $.ajax({
                    type: 'POST',
                    url: ajaxurl,
                    data: data,
                    success: function(response) {
                        console.log(data, 'Данные которые я тебе отправил')
                        console.log(response, 'Ответ сервера')
                    },
                    error: function(errorThrown){
                        console.log(data, 'Данные которые я тебе отправил')
                        console.log(errorThrown, 'Ответ сервера')
                    }
                });
            }
        }


        currentStep.classList.add('hidden');
        currentStep.classList.remove('c-budget-steps--active');

        const activeStep = document.getElementById(`step-${next}`);
        activeStep.classList.remove('hidden');
        activeStep.classList.remove('c-budget-steps--prev');

        if (this.dataset.is_prev) {
            activeStep.classList.add('c-budget-steps--prev');
        }

        activeStep.classList.add('c-budget-steps--active');

        setTimeout(() => {
            currentStep.classList.remove('hidden');
        }, 0);

        const number = activeStep.dataset.number;
        const all = activeStep.dataset.all;
        const percent = parseInt(number / all * 100);

        progress.style.width = `${percent}%`;
    });
}
