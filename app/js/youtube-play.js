const tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
const firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

$('.video__cars-videos-view-play').on('click', function () {
   const $this = $(this);

   const code = $this.data('code');
   const id = `video-${code}`;

   const iframeWrap = document.createElement('div');

   iframeWrap.setAttribute('id', id);

   $this
    .closest('.video__cars-videos')
    .find('.video__cars-videos-view')
    .html(iframeWrap)
   ;

   new YT.Player(id, {
      height: '326',
      width: '561',
      videoId: code,
      events: {
         'onReady': function onPlayerReady(event) {
            event.target.playVideo();
         },
      }
   });
});
