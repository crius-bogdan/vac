let LoanVal = 18;
let AmountVal = 15000;
const inputsVariants = document.querySelectorAll('input[name="credit-variant"]');
const GetProjectPayment = () => {
    const radioVal = document.querySelector('input[name="credit-variant"]:checked').value.toUpperCase();
    let amount = AmountVal / 100;

    if(radioVal == 'POOR') {
        amount = (amount * 15.9) + AmountVal;
    }
    if(radioVal == 'AVERAGE') {
        amount = (amount * 9.9 ) + AmountVal;
    }
    if(radioVal == 'GOOD') {
        amount = (amount * 4.9 ) + AmountVal;
    }

    const amountPerMonth = (amount/LoanVal).toFixed();
    const amountPerWeek = amount/LoanVal/4;
    const amountPayment = (amountPerWeek*2).toFixed()


    document.querySelector('.payment').innerHTML = '$ ' + amountPayment.toLocaleString('en');
    document.querySelector('.month-payment').innerHTML = '$ ' + amountPerMonth.toLocaleString('en');
    document.querySelector('.week-payment').innerHTML = '$ ' + amountPerWeek.toFixed().toLocaleString('en');

}

for(let input of inputsVariants) {
    input.addEventListener('change', () => {
        GetProjectPayment()
    })
}
const rangeSliderMonth = document.getElementById('slider-range-months');

noUiSlider.create(rangeSliderMonth, {
    start: [18],
    step: 1,
    range: {
        'min': [1],
        'max': [96]
    }
});

const rangeSliderValueMonth = document.getElementById('months-count');

rangeSliderMonth.noUiSlider.on('update', (values, handle) => {
    rangeSliderValueMonth.innerHTML = parseInt(values[handle]);
    LoanVal = parseInt(values[handle]);
    GetProjectPayment()
});

const rangeSliderAmount = document.getElementById('slider-range-amount');

noUiSlider.create(rangeSliderAmount, {
    start: [15000],
    decimals: 0,
    step: 1,
    thousand: ' ',
    range: {
        'min': [1],
        'max': [50000]
    }
});

const rangeSliderValueAmount = document.getElementById('amount');

rangeSliderAmount.noUiSlider.on('update', (values, handle) => {
    rangeSliderValueAmount.innerHTML = parseInt(values[handle]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
    AmountVal = parseInt(values[handle])
    GetProjectPayment()
});


