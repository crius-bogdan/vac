(function(doc) {
    const dataset = 'dataset',
        clas = 'classList',
        activeClass = 'active',
        parentNode = 'parentNode',
        text = 'textContent',
        style = 'style',
        checked = 'checked',
        addClass = 'add',
        removeClass = 'remove';

    function match(element, className) {
        return element[clas].contains(className);
    }
    function getCoords(elem) {
        var box = elem.getBoundingClientRect();

        return box.top + pageYOffset;
    }

    window.onload = () => {
        AOS.init({
            offset: 200
        });
        doc.addEventListener('click', (e) => {
            const target = e.target;

            if(match(target, 'contact-modal-btn')) {
                doc.querySelector('.contact-form')[clas][addClass]('open')
                doc.querySelector('.overlay')[clas][addClass]('show')
                doc.body[clas][addClass]('overflow--active');
            }
            if(match(target, 'close-btn') || match(target, 'overlay')) {
                doc.querySelector('.contact-form')[clas][removeClass]('open')
                doc.querySelector('.overlay')[clas][removeClass]('show')
                doc.body[clas][removeClass]('overflow--active');
            }
        })
        if(window.innerWidth > 768 && doc.querySelector('.lexus img')) {
            doc.addEventListener('scroll', (e) => {
                const sctollPosition = window.scrollY;

                doc.querySelector('.lexus img').style.transform = `translateX(-${sctollPosition / 7}px)`;
                doc.querySelector('.honda img').style.transform = `translateX(${sctollPosition / 7}px)`;
            });
        }
    };

    let swiper = new Swiper('.c-home__wrapper-content-carusel .swiper-container', {
        loop: true,
        navigation: {
            nextEl: '.c-home__wrapper-content-carusel-arrow-right',
            prevEl: '.c-home__wrapper-content-carusel-arrow-left',
        },
        on: {
            transitionEnd() {
                const text = document.querySelector('.c-home__wrapper-content-carusel .swiper-slide-active').dataset.text
                document.querySelector('.c-home__wrapper-content-text-btn').textContent = text
            }
        }
    });
    $(".wpcf7").on('wpcf7:mailsent', (e) => {
        e.preventDefault()
        $('.contact-form').addClass('great')
        setTimeout(() => {
            $('.contact-form').removeClass('great open')
            doc.body[clas][removeClass]('overflow--active');
        }, 2000)
    });
})(document)
